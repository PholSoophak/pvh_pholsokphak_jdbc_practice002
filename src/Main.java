import javax.print.attribute.standard.PresentationDirection;
import javax.xml.transform.Result;
import java.sql.*;
import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);
    static Person getInputFromUser(){

        System.out.print("Enter name: ");
        String name = scanner.next();
        System.out.print("Enter address: ");
        String address = scanner.next();

        return new Person(name ,address);
    }

    static void insertIntoDB(Connection connection){
        System.out.print("Enter id to update:");
        int id= scanner.nextInt();
        String insertQuery = "INSERT INTO homeworktb VALUES(default, ?, ?)";

        //update
        String updateQuery = "UPDATE homeworktb SET name=? , address=? WHERE id=?";
        Person p =getInputFromUser();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(updateQuery);
            preparedStatement.setString(1,p.getName());
            preparedStatement.setString(2, p.getAddress());
            preparedStatement.setInt(3,id);
            int row = preparedStatement.executeUpdate();
            if (row > 0){
                System.out.println("Successfully");
            }
            else System.out.println("Failed");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        ConnectionDatabase connectionDatabase = new ConnectionDatabase();
        Connection connection = connectionDatabase.getConnection();
        insertIntoDB(connection);

    }
}