import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDatabase {
    Connection getConnection(){
        Connection connection =null;
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/hwdb","postgres","041299");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }
}
